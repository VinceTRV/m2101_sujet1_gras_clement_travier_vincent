GCC = gcc
SOURCES = $(wildcard *.c)
BINAIRES = $(patsubst %.c,%.o,${SOURCES})


all:*.o main

main: ${BINAIRES}
	${GCC} $^ -o $@

%.o: %.c %.h
	${GCC} -c $<
	
clean:
	rm main
	rm *.o
	rm ip.txt
	#Pour enlever le fichier texte lors de la mise à jour du programme
