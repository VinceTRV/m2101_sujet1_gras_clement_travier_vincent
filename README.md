# M2101_SUJET1_GRAS_CLEMENT_TRAVIER_VINCENT

//ecrireIp :
C'est la fonction première qui permet a l'utilisateur d'entrer son ip et de le stocker dans un tableau.

//verificationFormat
Cette fonction test si il y a bien le nombre de . et de / attendu, il vérifie aussi qu'on est bien 18 caractères.

//extraction
Le rôle de cette fonction est de séparer nos différente partie et de mettre la partie 1 dans la première case du nouveau tableau
la deuxième dans la seconde ect

//conversion
Nous faisons passer nos caractère en int afin de pouvoir les manipuler plus facilement

//extractionClasse
Dans cette méthode nous allons donner la classe de l'ip et dire si elle est public, privé, réservé

//localHostBroadcast
La méthode permet de dire si notre ip est de type localHost, broadcast où autre.

Le main contient 3 variables : 
	char * separate_ip[5] = {'0'}; //Un pointeur de tableau utilisé pour séparé les ip
	char ip [TAILLE_IP] = {'0'}; //Le tableau de base ou l'utilisateur entre l'ip
	int valIp[5] = {'0'}; //Un tableau de taille 5 contenant les parties de notre ip séparé