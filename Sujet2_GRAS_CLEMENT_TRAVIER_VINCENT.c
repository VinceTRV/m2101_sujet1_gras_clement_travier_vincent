//le programme reconnait les classe A,B,C,D,E / le multicast, localHost, ip réservé / vérifie que le format corespond a une ip.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define TAILLE_IP 17

void ecrireIp (char * ip){
    printf("Entrez votre ip : ");
    char* ligne = NULL;
    int lgLigne = 0;
    size_t taille = 0;
    lgLigne = getline(&ligne, &taille, stdin);
    strcpy(ip,ligne);
}

int verificationFormat (char * ip){
    //test condition longueur
    int taille = strlen(ip);

    //test occurence char '/'
    int nbocc1 = 0;
    char car = '/';
    for (int c = 0 ; c <= taille ; c++){
        if (ip[c] == car){
            nbocc1 ++;
         }
    }
    
    //test occurence char '.'                                                                                                                                                                                                           //test occurence nombre '.'
    int nbocc2 = 0;
    char car2 = '.';
        for (int c = 0 ; c <= taille ; c++){
        	if (ip[c] == car2){                                                                                                                                                                                if (ip[c] == car){
            	nbocc2 ++;
        	}
        }
    }

    //assemblage de tous les tests pour retourner 1 (format correct) ou 0 (format incorect)
        if (nbocc2 == 3 && nbocc1 == 1 && taille <= TAILLE_IP){
                printf("Votre format d'ip est correct");
                return 1;
        }
        else {
                printf("Votre format d'ip est incorrect");
                return 0;
        }
}

void extraction(char * ip, char ** separate_ip){
	char * pch;
	pch = strtok(ip,"./");
	int i = 0;
	while (pch != NULL){
		separate_ip[i] = pch;
		pch = strtok(NULL, "./");
		i++;
	}
}

int conversion(int *valeur, char ** tabloIp){
	for (int i = 0; i < 5; i++){
		valeur[i] = strtol(tabloIp[i], NULL, 10);
	}
	for (int i = 0; i < 4; i++){
		if (valeur[i] >= 0 && valeur[i] <= 255){
			printf("votre ip comporte des valeurs comprise entre 0 et 255\n");
			if (valeur[4] <= 32){
				return 1;
			}
			else{
				return 2;
				printf("votre masque comporte des valeurs non comprise entre 0 et 32\n");
			}
		}
		else {
			return 0
			printf("votre ip comporte des valeurs non comprise entre 0 et 255\n");
		}
	}
}

void extractionClasse (int *ipSepare){
	if (ipSepare[1] >= 0 && ipSepare[1] <= 126 && ipSepare[1] != 10){
		printf("Votre ip est public et de classe A");
	}
	else if (ipSepare[1] >= 127 && ipSepare[1] <= 191 && ipSepare[1] != 172){
		printf("Votre ip est public et de classe B");
	}
	else if (ipSepare[1] >= 192 && ipSepare[1] <= 223 && ipSepare[1] != 192){
		printf("Votre ip est public et de classe C");
	}
	else if (ipSepare[1] == 10){
		printf("Votre ip est privé et de classe A");
	}
	else if (ipSepare[1] == 172){
		printf("Votre ip est privé et de classe B");
	}
	else if (ipSepare[1] == 192){
		printf("Votre ip est privé et de classe C");
	}
	else if (ipSepare[1] >= 224 && ipSepare[1] <= 239 && ipSepare[5] == 0){
		printf("Votre ip est de type multicast et de classe D");
	}
	else if (ipSepare[1] => 240 && ipSepare[1 <= 255]){
		printf("votre ip est de type réservé et de classe E");
	}
	else{
		printf("Votre ip comporte un problème");
	}
}

void localHostBroadcast (int *valeur){
	if (valeur[1] == 0 && valeur[2] == 0 && valeur[3] == 0 && valeur[4] == 0 ){
		printf("Votre ip est de type localHost");
		return 1;
	}
	else if (valeur[1] == 255 && valeur[2] == 255 && valeur[3] == 25 && valeur[4] == 25){
		printf("Votre ip est de type broadcast");
		return 2;
	}
	else {
		printf("Votre ip n'est pas broadcast ou localHost");
	}
}



void main (){
	char * separate_ip[5] = {'0'};
	char ip [TAILLE_IP] = {'0'};
	int valIp[5] = {'0'};s

	ecrireIp(ip);
	if (verificationFormat(ip) == 0){
		while (verificationFormat(ip) == 0){
			ecrireIp(ip);
			verificationFormat(ip);
		}
	else {
		extraction(ip, separate_ip);

		if (conversion(valIp, separate_ip) == 0){
			while (conversion(valIp, separate_ip) != 1){
			ecrireIp(ip);
			verificationFormat(ip);
			extraction(ip, separate_ip);
			conversion(valIp, separate_ip);
		}
		else {
				extractionClasse(separate_ip);
				localHostBroadcasts(separate_ip);
			}
		}
	}
}


/* VERSION 1
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define TAILLE_IP 17

char ip [TAILLE_IP]
char p1 [3];
char p2 [3];
char p3 [3];
char p4 [3];
char masque [2];

void EcrireIp (){
    printf("Entrez votre ip : ");
    char* ligne = NULL;
    int lgLigne = 0;
    size_t taille = 0;
    lgLigne = getline(&ligne, &taille, stdin);
    strcpy(ip,ligne);
}

bool VerificationFormat (){
	//test condition longueur
	int taille = strlen(ip);

	//test occurence nombre de '/'
	int nbocc1 = 0;
	char car = '/'
	for (int c = 0 ; c < taille ; c++){
        if (ip[c] == car){
            nbocc1 ++;         
        }
    }

    //test occurence nombre '.'
    nbocc2 = 0
    char car = '.'
	for (int c = 0 ; c < taille ; c++){
    	if (ip[c] == car){
            nbocc ++;         
		}
    }

    //assemblage de tous les tests pour retourner 1 (format correct) ou 0 (format incorect)
	if (nbocc2 = 4 && nbocc1 = 1 && taille <= 18){
		printf("Votre ip est correct")
		return 1;
	else {
		printf("Votre ip est incorrect")
		return 0
	}
}
//Maximum ; masque max

void Extraction(){
	int nombreChiffre = 0;
	int nombrePoint = 0;
	for (int i = 0 ; i < TAILLE_IP ; i++){
		if (ip[i] != '.' && ip[i] != '!'){
			nombreChiffre++ ;
		}
		else if (ip[i] = '.'){
			nombrePoint++ 
			if (nombreChiffre = 3){
				char tampon [3] = {ip[i - 3], ip[i - 2], ip[i - 1]}
				testNombreP(nombrePoint, tampon);
			}
			else if (nombreChiffre = 2){
				char tampon [3] = {ip[i - 2], ip[i - 1]}
				testNombreP(nombrePoint, tampon);
			}
			else {
				char tampon [3] = {ip[i - 1]}
				testNombreP(nombrePoint, tampon);
			}
		}
		else {
			if (ip[i + 2] != NULL) {
			masque [2] = {ip[i + 1], ip[i + 2]}
			}
			else {
				masque[2] = {ip[i + 1]}
			}
		}

	}
}

//solution brouillon, j'avais un problème pour savoir combien de char avant chaque point (3, 2 ou 1) 
//mais aussi pour savoir si j'était après le premier, deuxieme, troisième ou dernier point (pour pouvoir les ranger dans la sous partie correspondante)
void testNombreP (int nombreP, char tableau[3]){
	if (nombreP = 1){
		strcpy(p1, tampon);
	}
	else if (nombreP = 2) {
		strcpy(p2, tampon);
	}
	else if (nombreP = 3{
		strcpy(p3, tampon); 
	}
	else {
		strcpy(p4, tampon);
	}
}

void Convertion (){

}

void main (){
	EcrireIp();
	for (i = 0 ; i < TAILLE_IP ; i++){
        printf("%c", ip[i])
    }
}